/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author Javier
 */
public class Ventana extends javax.swing.JFrame{
    
    public Ventana(){
        this.setTitle("Batman");
        this.setSize(600, 600);
        this.setDefaultCloseOperation(3);
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        
       Hilo h;
       h = new Hilo(this);
       h.start();
    
   
    }

   
    
    public void dibujarB(){
        java.awt.Graphics g;
        g = this.getGraphics();
        g.setColor(Color.BLACK);
        //g.drawRect(50, 200, 150, 150);
        g.fillRect(51, 160, 150, 150);
        
        
        
        g.setColor(Color.BLACK);
        java.awt.Polygon p;
        p = new java.awt.Polygon();
        p.addPoint(100, 100);
        p.addPoint(100, 200);
        p.addPoint(50, 200);
        g.fillPolygon(p);
        g.drawPolygon(p);
        
        java.awt.Polygon q;
        q = new java.awt.Polygon();
        q.addPoint(200, 100);
        q.addPoint(200, 200);
        q.addPoint(100, 200);
        g.fillPolygon(q);
        g.drawPolygon(q);
        
        
       g.setColor(Color.white);
       g.fillOval(78, 190 , 20, 20);
       
       g.setColor(Color.GRAY);
       g.fillRect(53, 260, 60, 50);
       
       g.setColor(Color.BLACK);
       g.setFont(new Font("Serif", Font.BOLD,26));
       g.drawString("Batman:\n The Last Knight", 250, 200);
        
       
        
     
    }
    
    public void DibujarCar(int w){
       java.awt.Graphics g;
       g = this.getGraphics();
       g.setFont(new Font("Serif", Font.BOLD,20));
       g.drawString("Cargando", 250, 400);
       g.drawRect(250+w, 450, 50, 20);
       
    }
    
    
}
