package juego;

import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;

/**
 *
 * @author Javier
 */
public class Escenario extends javax.swing.JFrame {

    boolean band = false;
    boolean band1 = false;

    java.util.ArrayList<Obstaculo> obstaculos = new java.util.ArrayList<Obstaculo>();

    public Escenario() {
        this.setTitle("");
        this.setSize(1000, 530);
        this.setDefaultCloseOperation(3);
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        Sonido s = new Sonido();
        s.play("src/dark.WAV", false, 0.0f);

        Batman b = new Batman(50, 400, this);

        this.add(b);

        Banqueta p;
        p = new Banqueta(this);
        
        Banqueta p2;
        p2 = new Banqueta(this);
        p2.Desplazar(610, 420);
        
        Banqueta p3;
        p3 = new Banqueta(this);
        p3.Desplazar(710, 420);

        ImageIcon i = new ImageIcon(getClass().getResource("EscenarioBien.jpg"));
        i = new ImageIcon(i.getImage().getScaledInstance(2000, 500, java.awt.Image.SCALE_DEFAULT));
        javax.swing.JLabel c;
        c = new javax.swing.JLabel();
        c.setSize(2000, 500);
        c.setLocation(0, 0);
        c.setOpaque(true);
        c.setIcon(i);
        this.add(c);

        java.awt.event.KeyListener k;
        k = new java.awt.event.KeyListener() {
            boolean bandera = false;

            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                Sonido s1;
                Sonido s2;

                if (band == false) {

                    if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                        b.caminar(Batman.OpcionesDireccion.DERECHA);

                        if (band1 == false) {
                            s1 = new Sonido();
                            s.play("src/KNCKPTN1.WAV", false, 0.0f);
                            band1 = true;
                        }
                    }
                    if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                        b.caminar(Batman.OpcionesDireccion.IZQUIERDA);
                        if (band1 == false) {
                            s1 = new Sonido();
                            s.play("src/KNCKPTN1.WAV", false, 0.0f);
                            band1 = true;
                        }
                    }

                    if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                        System.out.println("esp");
                        b.saltar(Batman.OpcionesDireccion.DERECHA);

                        if (bandera == false) {
                            s1 = new Sonido();
                            s.play("src/BONK.WAV", false, 0.0f);
                            band = true;
                        }
                    }
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {

                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    band = false;
                    band1 = false;

                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    band = false;
                    band1 = false;

                }

                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    bandera = false;
                }
            }
        };
        this.addKeyListener(k);

         MoverEscenario me;
         me = new MoverEscenario(c);
         me.start();
    }

}
