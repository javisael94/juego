/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import javax.swing.ImageIcon;

/**
 *
 * @author Javier
 */
public class Batman extends javax.swing.JLabel {

    public final int ALTO = 80;
    public final int ANCHO = 80;

    ImageIcon sprites_batman[];
    ImageIcon sprites_batmanizq[];
    int pos_actual;
    
    Escenario v;

    public Batman(int x, int y, Escenario v) {
        this.setSize(ALTO, ANCHO);
        this.setLocation(x, y);
        this.setOpaque(false);
        this.v = v;

        ImageIcon i = new ImageIcon(getClass().getResource("Batman1.png"));
        i = new ImageIcon(i.getImage().getScaledInstance(ALTO, ANCHO, java.awt.Image.SCALE_DEFAULT));

        ImageIcon i2 = new ImageIcon(getClass().getResource("Batman2.png"));
        i2 = new ImageIcon(i2.getImage().getScaledInstance(ALTO, ANCHO, java.awt.Image.SCALE_DEFAULT));

        ImageIcon i3 = new ImageIcon(getClass().getResource("Batman3.png"));
        i3 = new ImageIcon(i3.getImage().getScaledInstance(ALTO, ANCHO, java.awt.Image.SCALE_DEFAULT));

        sprites_batman = new ImageIcon[3];
        sprites_batman[0] = i;
        sprites_batman[1] = i2;
        sprites_batman[2] = i3;

        ImageIcon i4 = new ImageIcon(getClass().getResource("BatmanIzq1.png"));
        i4 = new ImageIcon(i4.getImage().getScaledInstance(ALTO, ANCHO, java.awt.Image.SCALE_DEFAULT));

        ImageIcon i5 = new ImageIcon(getClass().getResource("BatmanIzq2.png"));
        i5 = new ImageIcon(i5.getImage().getScaledInstance(ALTO, ANCHO, java.awt.Image.SCALE_DEFAULT));

        ImageIcon i6 = new ImageIcon(getClass().getResource("BatmanIzq3.png"));
        i6 = new ImageIcon(i6.getImage().getScaledInstance(ALTO, ANCHO, java.awt.Image.SCALE_DEFAULT));

        sprites_batmanizq = new ImageIcon[3];
        sprites_batmanizq[0] = i4;
        sprites_batmanizq[1] = i5;
        sprites_batmanizq[2] = i6;

        this.setIcon(i);

    }

    enum OpcionesDireccion {

        DERECHA, IZQUIERDA, ARRIBA, ABAJO
    }

    public void caminar(OpcionesDireccion direccion) {
        if (direccion == OpcionesDireccion.DERECHA) {
            if (!estaChocando(v, getX()+1,getY())) {
                
            
                this.setLocation(this.getX() + 5, this.getY());
                if (pos_actual < 2) {
                    pos_actual++;
                } else {
                    pos_actual = 0;
                }
                this.setIcon(sprites_batman[pos_actual]);
            }
        }
        if (direccion == OpcionesDireccion.IZQUIERDA) {
            if (!estaChocando(v, getX()+5,getY())) {
            this.setLocation(this.getX() - 5, this.getY());
            if (pos_actual < 2) {
                pos_actual++;
            } else {
                pos_actual = 0;
            }
            this.setIcon(sprites_batmanizq[pos_actual]);
        }
        }
            
    }
    

    public void saltar(OpcionesDireccion direccion) {
        SaltaBatman s = new SaltaBatman(this, direccion);
        s.start();
    }

    public boolean estaChocando(Escenario v, int x, int y) {
        for (Obstaculo obs : v.obstaculos) {
            if (obs.poligono.contains(x, y)) {
                return true;
            }
            if (obs.poligono.contains(x + getWidth(), y)) {
                return true;
            }
            if (obs.poligono.contains(x + getWidth(), y + getHeight())) {
                return true;
            }
            if (obs.poligono.contains(x, y + getHeight())) {
                return true;
            }
            if (obs.poligono.contains(x + obs.getWidth() / 2, y + getHeight())) {
                return true;
            }
            if (obs.poligono.contains(x + getWidth(), y + obs.getHeight() / 2)) {
                return true;
            }
            if (obs.poligono.contains(x + obs.getWidth() / 2, y)) {
                return true;
            }
            if (obs.poligono.contains(x, y + obs.getHeight() / 2)) {
                return true;
            }
        }
        return false;
    }

}
