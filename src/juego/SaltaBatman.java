/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author Javier
 */
public class SaltaBatman extends  Thread{
    
    public static boolean ocupado = false;
    
    Batman b;
    Batman.OpcionesDireccion direccion;
    
    public SaltaBatman(Batman b, Batman.OpcionesDireccion direccion){
        this.b = b;
        this.direccion = direccion;
        
    }
    
    public void run(){
        try{
            if(!ocupado){
                ocupado=true;
                int pos_inicial_y = b.getY();
                int pos_inicial_x = b.getX();
                
                for(int i=180; i<=360; i+=10){
                    int x=0,y;
                    if(direccion==Batman.OpcionesDireccion.DERECHA){
                            x = (i-180)/4 + pos_inicial_x ;
                       
                    }
                    if(direccion==Batman.OpcionesDireccion.IZQUIERDA)  {                    
                            x = -(i-180)/4 + pos_inicial_x ;
                        
                    
                    }
                    y = (int)(Math.sin(i/57.2958) * 60.0) + pos_inicial_y;
                    b.setLocation(x,y);
                    sleep(100);
                    
                }  
                }
                ocupado=false;
            }
        catch(Exception e){
            
        }
    }
    
}
