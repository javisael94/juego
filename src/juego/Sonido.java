/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import java.io.File;
import javax.sound.sampled.*; 
import java.io.*; 

/**
 *
 * @author Javier
 */
public class Sonido extends javax.swing.JFrame {
    Clip ol;
    
    public void stop(){
        ol.stop();
    }
    public void play(String ruta, boolean repetir, float volumen_a_reducir){
      File sf=new File(ruta); 
      AudioFileFormat aff; 
      AudioInputStream ais; 
 
      try{ 
         aff=AudioSystem.getAudioFileFormat(sf); 
         ais=AudioSystem.getAudioInputStream(sf); 
         AudioFormat af=aff.getFormat(); 
         DataLine.Info info = new DataLine.Info( 
                        Clip.class, 
                        ais.getFormat(), 
                        ((int) ais.getFrameLength() * 
                        af.getFrameSize())); 
         ol = (Clip) AudioSystem.getLine(info); 
         ol.open(ais); 
         if(!repetir){
             ol.loop(0); 
         }else{
         ol.loop(Clip.LOOP_CONTINUOUSLY);//Clip.LOOP_CONTINUOUSLY
         }
       FloatControl gainControl
               = (FloatControl) ol.getControl(FloatControl.Type.MASTER_GAIN);
       gainControl.setValue(volumen_a_reducir);
      }catch(Exception e){
          System.out.println(e.getMessage());
      }
   } 
}
